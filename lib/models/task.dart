import 'package:cloud_firestore/cloud_firestore.dart';

class Task {
  final String name;
  final id;
  bool isDone;

  Task({this.name, this.isDone = false, this.id});

  factory Task.fromFirestore(DocumentSnapshot doc) {
    return Task(
      name: doc.data['name'],
      isDone: doc.data['isDone'],
      id: doc.documentID
    );
  }

  void toggleDone() {
    isDone = !isDone;
  }
}
