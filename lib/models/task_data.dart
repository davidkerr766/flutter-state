import 'package:flutter/cupertino.dart';
import 'task.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TaskData extends ChangeNotifier {
  final _db = Firestore.instance;

  Stream<List<Task>> getTasks() {
    return _db.collection('tasks').snapshots().map((snapshot) => snapshot
        .documents
        .map((document) => Task.fromFirestore(document))
        .toList());
  }

  void addTask(String newTaskTitle) {
    _db.collection('tasks').add({'name': '$newTaskTitle', 'isDone': false});
  }

  void updateTask(Task task) {
    task.toggleDone();
    _db
        .collection('tasks')
        .document(task.id)
        .updateData({'isDone': task.isDone});
  }

  void deleteTask(Task task) {
    _db.collection('tasks').document(task.id).delete();
  }
}
