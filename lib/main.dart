import 'package:flutter/material.dart';
import 'package:flutter_state/models/task_data.dart';
import 'package:provider/provider.dart';
import 'screens/tasks_screen.dart';
import 'models/task_data.dart';
import 'models/task.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final taskData = TaskData();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => taskData,
        ),
        StreamProvider(
          create: (context) => taskData.getTasks(),
          initialData: [Task(name: 'loading...')],
        )
      ],
      child: MaterialApp(
        home: TasksScreen(),
      ),
    );
  }
}
