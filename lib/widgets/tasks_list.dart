import 'package:flutter/material.dart';
import 'package:flutter_state/models/task_data.dart';
import 'task_tile.dart';
import 'package:provider/provider.dart';
import '../models/task.dart';
import '../models/task_data.dart';

class TasksList extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Consumer<List<Task>>(
      builder: (context, tasks, child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final task = tasks[index];
            return TaskTile(
              taskTitle: task.name,
              isChecked: task.isDone,
              checkboxCallback: (checkboxState) {
                Provider.of<TaskData>(context, listen: false).updateTask(task);
              },
              longPressCallback: () {
                Provider.of<TaskData>(context, listen: false).deleteTask(task);
              },
            );
          },
          itemCount: tasks.length,
        );
      },
    );
  }
}
